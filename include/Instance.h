/*
 * Copyright 2017 Giret
 * */

#ifndef INCLUDE_INSTANCE_H_
#define INCLUDE_INSTANCE_H_

#include "Const.h"
#include <cstdint>
#include <vector>

class Instance {
    
 private:
    const int64_t id;  // the identifier of the instance
    const int64_t source;  // the source vertex identifier of the instance
    const int64_t target;  // the target vertex identifier of the instance
    std::vector<int> maxValuesConstrained; // the value maximum of the constrained ressources

 public:
    /**
     * Constructor of an instance object
     * @param _id the identifier of the instance
     * @param _source the source vertex identifier of the instance
     * @param _target the target vertex identifier of the instance
     */
    Instance(const int64_t _id, const int64_t _source, const int64_t _target, const std::vector<int> _maxValuesConstrained) :
            id(_id), source(_source), target(_target) {
                maxValuesConstrained = std::vector<int>(_maxValuesConstrained);
            }

    std::vector<int> getMaxValuesConstrained() const{
        return maxValuesConstrained;
    }
    /**
     * Getter of the identifier of the instance
     * @return the identifier of the instance
     */
    int64_t getId() const {
        return id;
    }

    /**
     * Getter of the source vertex identifier of the instance
     * @return the source vertex identifier of the instance
     */
    int64_t getSource() const {
        return source;
    }

    /**
     * Getter of the target vertex identifier of the instance
     * @return the target vertex identifier of the instance
     */
    int64_t getTarget() const {
        return target;
    }
};

#endif  // INCLUDE_INSTANCE_H_
