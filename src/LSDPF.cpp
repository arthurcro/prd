/*
 * Copyright 2017 Giret
 * */

#include "../include/LSDPF.h"

LSDPF::LSDPF(const Graph *_graph, const int64_t _sourceId, const int64_t _targetId, std::vector<int> _maxValuesConstrained) :
        graph(_graph->getInstance()),
        source(_graph->getVertex(_sourceId)),
        target(_graph->getVertex(_targetId)),
        labelId(-1),
        criteriaValues(std::vector<std::vector<std::vector<double>>>(
                                                               firstPhaseWeights.size(),
                                                               std::vector<std::vector<double>>(
                                                                                                num_vertices(_graph->getInstance()),
                                                                                                std::vector<double>(NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED + NB_CRITERIA_MIN_MAX,std::numeric_limits<double>::max())))),
        vertexLabelList(new vertex_label_list_t[_graph->getInstance().m_vertices.size()])
{
            maxValuesConstrained = _maxValuesConstrained;
    onTargetReached.connect([this] {
        ++nbTimesTargetReached;
        if (nbTimesTargetReached == firstPhaseWeights.size()) {
            for (u_int8_t n = 0; n < firstPhaseWeights.size(); ++n) {
                for (u_int8_t k = 0; k < NB_CRITERIA_COST; ++k) {
                    worstCriteriaValues[k] = std::max(worstCriteriaValues[k], criteriaValues[n][source][k]);
                }
                for( u_int8_t i = 0; i < NB_CRITERIA_CONSTRAINED; i++){
                    //Worst for constrained criteria = max value constrained
                    worstCriteriaValues[NB_CRITERIA_COST + i] = maxValuesConstrained[i];
                }
                //For the MaxMin criteria, the worst is the minimum value found after the mono search
                for( u_int8_t i = 0; i < NB_CRITERIA_MIN_MAX; i++){
                    worstCriteriaValues[NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED + i] = std::max(worstCriteriaValues[NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED + i], criteriaValues[n][source][NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED + i]);
                }
            }

            allTargetReached = true;
            onTargetReached.disconnect_all_slots();
        }
    });
}

LSDPF::~LSDPF() {
    criteriaValues.clear();
    maxValuesConstrained.clear();
    delete[] vertexLabelList;
    nbTimesTargetReached = 0;
    allTargetReached = false;
    worstCriteriaValues = std::vector<double>(NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED + NB_CRITERIA_MIN_MAX, 0.);
}

void LSDPF::runReversedDijkstra(const boost::reverse_graph<graph_t> &reverseGraph, const u_int8_t n) {
    using boost::dijkstra_shortest_paths;
    using boost::make_iterator_property_map;
    using boost::make_transform_value_property_map;

    // Initializing boost graph parameters
    std::vector<double> distances(num_vertices(graph));
    std::vector<int64_t> predecessors(num_vertices(graph));

    auto predecessorMap = make_iterator_property_map(predecessors.begin(), get(&Vertex::id, reverseGraph));
    auto distanceMap = make_iterator_property_map(distances.begin(), get(&Vertex::id, reverseGraph));
    auto weightMap = make_transform_value_property_map([_n = n](const Edge &e) -> double {
        // Getting edge linear combination value according to a set of defined weights
        double lcValue = 0.;
        for (u_int8_t k = 0; k < NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED + NB_CRITERIA_MIN_MAX; ++k) {
            lcValue += e.getCriteriaValues()[k] * firstPhaseWeights[_n][k];
        }
        return lcValue;
    }, boost::get(boost::edge_bundle, reverseGraph));

    // Initializing criteria values
    criteriaValues[n][target] = std::vector<double>(NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED + NB_CRITERIA_MIN_MAX, 0.);
    
    // Running the reversed Dijkstra one-to-all search
    try {
        dijkstra_shortest_paths(reverseGraph,
                                target,
                                predecessor_map(predecessorMap)
                                        .distance_map(distanceMap)
                                        .weight_map(weightMap)
                                        .vertex_index_map(boost::get(&Vertex::id, reverseGraph))
                                        .visitor(LSDPFDijkstraVisitor(source, criteriaValues[n], maxValuesConstrained)));
    } catch (int) {}  // Prevent Dijkstra cut
}

void LSDPF::runFirstPhase() {
    boost::timer::cpu_timer firstPhaseTimer;
    firstPhaseTimer.start();

    // Reversing the graph
    boost::reverse_graph<graph_t> reversedInstance = boost::make_reverse_graph(graph);

    // Running reversed Dijkstra searches in multiple threads
    boost::thread_group threadGroup;
    for (u_int8_t n = 0; n < firstPhaseWeights.size(); ++n) {
        threadGroup.create_thread(boost::bind(&LSDPF::runReversedDijkstra,
                                              this,
                                              boost::ref(reversedInstance),
                                              n));
    }
    threadGroup.join_all();
    
    firstPhaseTimer.stop();
    stats.firstPhaseTime = getElapsedTime(firstPhaseTimer);
}

int64_t LSDPF::getNextLabelId() {
    ++labelId;
    if (labelId == std::numeric_limits<int64_t>::max()) {
        throw std::invalid_argument("The number of created labels is too high");
    }
    return labelId;
}

template<typename Tag>
Label LSDPF::getNextLabel() {
    auto nextLabelIt = explorationQueue.get<Tag>().begin();
    Label label = *(nextLabelIt);
    explorationQueue.get<Tag>().erase(nextLabelIt);
    return label;
}

bool LSDPF::isDominated(const std::vector<double> &criteriaValues, const vertex_t vertex) const {
    // Getting labels of the vertex
    const auto &labelList = vertexLabelList[vertex].get<Label::ByCriteriaValues>();

    // Testing if the set of criteria values is dominated by a label
    auto labelListIt = labelList.upper_bound(criteriaValues);
    if (labelListIt == labelList.begin()) {
        return false;
    } else {
        --labelListIt;
        const std::vector<double> &labelCriteriaValues = labelListIt->getCriteriaValues();
        bool isDominated = true;
        
        for (u_int8_t k = 0; k < NB_CRITERIA_COST; ++k) {
            isDominated = isDominated && criteriaValues[k] >= labelCriteriaValues[k];
        }
        for (u_int8_t k = 0; k < NB_CRITERIA_MIN_MAX ; k++){
            isDominated = isDominated && criteriaValues[NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED + k] >= labelCriteriaValues[NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED + k];
        }
        return isDominated;
    }
}

void LSDPF::purgeDominated(const Label &label, std::vector<Label> &dominatedLabels) {
    // Getting labels of the vertex
    const auto &labelList = vertexLabelList[label.getVertex()].get<Label::ByCriteriaValues>();

    // Getting dominated labels
    const std::vector<double> &criteriaValues = label.getCriteriaValues();
    auto labelListIt = ++labelList.find(criteriaValues);
    while (labelListIt != labelList.end()) {
        const std::vector<double> &labelCriteriaValues = labelListIt->getCriteriaValues();
        bool isDominated = true;
        
        for (u_int8_t k = 0; k < NB_CRITERIA_COST; ++k) {
            isDominated = isDominated && labelCriteriaValues[k] >= criteriaValues[k];
        }
        for (u_int8_t k = 0; k < NB_CRITERIA_MIN_MAX ; k++){
            isDominated = isDominated && criteriaValues[NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED + k] <= labelCriteriaValues[NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED + k];
        }
        
        if (isDominated) {
            dominatedLabels.push_back(*labelListIt);
            labelListIt = vertexLabelList[label.getVertex()].get<Label::ByCriteriaValues>().erase(labelListIt);
        } else {
            ++labelListIt;
        }
    }
}
void LSDPF::addLabelToVertex(const Label &label) {
    // Getting the identifier of the vertex where to attach the new label
    const vertex_t vertex = label.getVertex();

    // Getting labels already attached to the vertex
    const auto &labelList = vertexLabelList[vertex].get<Label::ByCriteriaValues>();

    /*
     * Testing if a label with similar criteria values already exists
     * If it doesn't exist, add the new label
     * Else replace it with the new label
     */
    auto labelListIt = labelList.find(label.getCriteriaValues());
    if (labelListIt == labelList.end()) {
        vertexLabelList[vertex].insert(label);
        if (vertex == target) {
            ++stats.nbLabelsAddedToPF;
        }
    } else {
        vertexLabelList[vertex].replace(labelListIt, label);
    }
}

void LSDPF::runSecondPhase() {
    boost::timer::cpu_timer secondPhaseTimer;
    secondPhaseTimer.start();
    
    /*
     * Building the initial label
     * Adding initial label to the exploration queue
     * Adding initial label to source vertex label list
     */
    std::vector<double> initCrtieriaValues = std::vector<double>(NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED + NB_CRITERIA_MIN_MAX, 0.);
    std::vector<double> initProjectedCriteriaValues = std::vector<double>(NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED + NB_CRITERIA_MIN_MAX, 0.);
    
    Label initialLabel(getNextLabelId(),
                       source,
                       -1,
                      initCrtieriaValues,
                       initProjectedCriteriaValues);
    addLabelToVertex(initialLabel);
    explorationQueue.insert(initialLabel);
    
    // Initializing the Pareto front
    for (u_int8_t n = 0; n < firstPhaseWeights.size(); ++n) {
        // Getting the criteria values obtained by the n-th search
        const std::vector<double> &values = criteriaValues[n][source];
        //Testing if new label exceeds one of the  max values for the constrained ressources
        bool exceedsRessource = false;
        for ( u_int8_t i = 0; i < NB_CRITERIA_CONSTRAINED ; i++){
            if (values[NB_CRITERIA_COST + i] > maxValuesConstrained[i]){
                exceedsRessource = true;
                break;
            }
        }
        // Testing if initial label will not be dominated by other ones
        if (!isDominated(values, target) && !exceedsRessource) {
            // Adding initial label to the Pareto front
            addLabelToVertex(Label(getNextLabelId(), target, initialLabel.getId(), values));
        }
    }

    // Running label-setting while exploration queue is not empty
    while (!explorationQueue.empty()) {
        // Selecting the next label to propagate
        Label currentLabel = getNextLabel<Label::ByProjectedCriteriaValues>();
        // Getting the current label criteria values
        const std::vector<double> &currentCriteriaValues = currentLabel.getCriteriaValues();

        // Processing all outgoing edges of the vertex associated to the current label
        out_edge_iterator_t edgeIt, outEdgesEnd;
        tie(edgeIt, outEdgesEnd) = out_edges(currentLabel.getVertex(), graph);
        for (; edgeIt != outEdgesEnd; ++edgeIt) {
            // Getting the criteria values of the edge
            const std::vector<double> &edgeCriteriaValues = graph[*edgeIt].getCriteriaValues();

            // Getting the current vertex
            const vertex_t &currentVertex = boost::target(*edgeIt, graph);
            // Initializing criteria values and projected criteria values of the label
            std::vector<double> values(NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED + NB_CRITERIA_MIN_MAX);
            std::vector<double> projectedValues(NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED + NB_CRITERIA_MIN_MAX, std::numeric_limits<double>::max());
            //For cost and constrained
            for (u_int8_t k = 0; k < NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED; ++k) {
                // Getting index of Dijkstra search considering only this criteria
                const u_int8_t n = weightsMap.find(k)->second;
                // Getting the criteria values of the new label
                const double &newValue = values[k] = currentCriteriaValues[k] + edgeCriteriaValues[k];

                // Getting the projected criteria values of the new label
                const double &projectedValue = criteriaValues[n][currentVertex][k];
                if (projectedValue != std::numeric_limits<double>::max()) {
                    projectedValues[k] = newValue + projectedValue;
                }
            }
            
            for (u_int8_t k = 0; k < NB_CRITERIA_MIN_MAX; k++){

                const u_int8_t n = weightsMap.find(NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED + k)->second;
                const double &newValue = values[NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED + k] = std::max(currentCriteriaValues[NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED + k] ,edgeCriteriaValues[NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED + k]);
                const double &projectedValue = criteriaValues[n][currentVertex][NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED + k];
                projectedValues[NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED + k] = std::max(newValue,projectedValue);
            }
            // Testing if new label will not be dominated on Pareto front
            //projected values vs target = IS_PROMISING
            //Testing if new label exceeds one of the  max values for the constrained ressources
            bool exceedsRessource = false;
            for ( u_int8_t i = 0; i < NB_CRITERIA_CONSTRAINED ; i++){
                if (projectedValues[NB_CRITERIA_COST + i] > maxValuesConstrained[i]){
                    exceedsRessource = true;
                    break;
                }
            }
            if (currentVertex == target || (!isDominated(projectedValues, target) && !exceedsRessource)){
                
                bool exceedsRessource = false;
                for ( u_int8_t i = 0; i < NB_CRITERIA_CONSTRAINED ; i++){
                    if (values[NB_CRITERIA_COST + i] > maxValuesConstrained[i]){
                        exceedsRessource = true;
                        break;
                    }
                }
                // Testing if new label will not be dominated on current vertex
                if (!isDominated(values, currentVertex) ) {
                    // Building the new label
                    Label newLabel(getNextLabelId(),
                                   currentVertex,
                                   currentLabel.getId(),
                                   values,
                                   projectedValues);

                    // Adding new label to current vertex
                    addLabelToVertex(newLabel);

                    // Purging dominated labels of current vertex
                    {
                        std::vector<Label> dominatedLabels;
                        purgeDominated(newLabel, dominatedLabels);
                        auto dominatedLabelIt = dominatedLabels.begin();
                        for (; dominatedLabelIt != dominatedLabels.end(); ++dominatedLabelIt) {
                            const auto &labelList = explorationQueue.get<Label::ById>();
                            auto labelIt = labelList.find(dominatedLabelIt->getId());
                            if (labelIt != labelList.end()) {
                                explorationQueue.get<Label::ById>().erase(labelIt);
                            }
                        }
                        dominatedLabels.clear();
                    }

                    // Updating dynamically the Pareto front
                    for (u_int8_t n : secondPhaseWeightIndexes) {
                        // Initializing criteria values of the projected label
                        std::vector<double> projectedLabelValues(NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED + NB_CRITERIA_MIN_MAX, std::numeric_limits<double>::max());
                        for (u_int8_t k = 0; k < NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED; ++k) {
                            const double &projectedValue = criteriaValues[n][currentVertex][k];
                            if (projectedValue != std::numeric_limits<double>::max()) {
                                projectedLabelValues[k] = values[k] + projectedValue;
                            }
                        }
                        
                        for (u_int8_t k = 0; k < NB_CRITERIA_MIN_MAX ; k++){
                            const double &projectedValue = criteriaValues[n][currentVertex][NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED + k];
                            projectedLabelValues[NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED + k] = std::max(values[NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED + k] , projectedValue);
                        }

                        //Testing if new label exceeds one of the  max values for the constrained ressources
                        bool exceedsRessource = false;
                        for ( u_int8_t i = 0; i < NB_CRITERIA_CONSTRAINED ; i++){
                            if (projectedLabelValues[NB_CRITERIA_COST + i] > maxValuesConstrained[i]){
                                exceedsRessource = true;
                                break;
                            }
                        }
                        // Testing if projected label will not be dominated on Pareto front
                        if (!isDominated(projectedLabelValues, target) && !exceedsRessource) {
                            // Building projected label
                            const Label projectedLabel(getNextLabelId(),
                                                       target,
                                                       newLabel.getId(),
                                                       projectedLabelValues);

                            // Adding projected label to Pareto front
                            const auto &labelList = vertexLabelList[target].get<Label::ByCriteriaValues>();
                            if (labelList.find(projectedLabelValues) == labelList.end()) {
                                vertexLabelList[target].insert(projectedLabel);
                                ++stats.nbLabelsAddedToPF;

                                // Purging dominated labels on Pareto front
                                std::vector<Label> dominatedLabels;
                                purgeDominated(projectedLabel, dominatedLabels);
                                dominatedLabels.clear();
                            }
                        }
                    }
                    std::cout << getParetoFront().size() << "\n";
                    // Adding new label to exploration queue if target not reached
                    if (currentVertex != target) {
                        explorationQueue.insert(newLabel);
                    }
                }
            }
        }
    }
    secondPhaseTimer.stop();
    stats.secondPhaseTime = getElapsedTime(secondPhaseTimer);
    stats.nbCreatedLabels = labelId;
    
}

unsigned int LSDPF::getElapsedTime(const boost::timer::cpu_timer &timer) const {
    return static_cast<unsigned int>(timer.elapsed().system + timer.elapsed().user) / 1000000;
}

void LSDPF::printStatistics() const {
    std::cout << "LSDPF statistics:" << std::endl
              << "Solutions: " << getParetoFront().size() << std::endl
              << "First phase time: " << stats.firstPhaseTime << "ms" << std::endl
              << "Second phase time: " << stats.secondPhaseTime << "ms" << std::endl
              << "Created labels: " << stats.nbCreatedLabels << std::endl
              << "Labels added to PF: " << stats.nbLabelsAddedToPF << std::endl
              << std::flush;
}

void LSDPF::printStatistics(const int64_t instanceId) const {
    const unsigned int elapsedTime = stats.firstPhaseTime + stats.secondPhaseTime;

    std::cout << "#" << instanceId << ":\t"
              << "|S| = " << getParetoFront().size() << ",\t"
              << "time = " << elapsedTime << "ms"
              << std::endl << std::flush;
}

