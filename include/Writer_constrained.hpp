//
//  Writer_constrained.hpp
//  label_setting
//
//  Created by Arthur Crocquevieille on 23/01/2018.
//

#ifndef Writer_constrained_hpp
#define Writer_constrained_hpp

#include <stdio.h>
#include <boost/filesystem.hpp>
#include <string>
#include <fstream>
#include "Graph.h"
#include "LSDPF_constrained.h"
#include "Instance_constrained.hpp"

class Writer_constrained {
    
private:
    std::string resultsFilesDir;  // the directory in which to write results files
    std::ofstream resultsFile;  // the results file stream
    
public:
    /**
     * Constructor of a writer object
     * @param _resultFilesDir the directory in which to write results
     */
    explicit Writer_constrained(const std::string &_resultFilesDir);
    
    /**
     * Destructor of a writer object
     */
    ~Writer_constrained();
    
    /**
     * Getter of the current time
     * @return the current time formatted
     */
    const std::string getCurrentTimeFormatted();
    
    /**
     * Method to append the resolved instance results to the results file
     * @param instance
     * @param lsdpf
     */
    void writeResults(const Instance_constrained &instance, const LSDPF_constrained *lsdpf);
    
    /**
     * Method to write the file containing the Pareto front of the resolved instance
     * @param instance
     * @param lsdpf
     */
    void writeParetoFrontFile(const Instance_constrained &instance, const LSDPF_constrained *lsdpf);
};
#endif /* Writer_constrained_hpp */
