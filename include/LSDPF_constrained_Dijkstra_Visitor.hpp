//
//  LSDPF_constrained_Dijkstra_Visitor.hpp
//  label_setting
//
//  Created by Arthur Crocquevieille on 18/01/2018.
//

#ifndef LSDPF_constrained_Dijkstra_Visitor_hpp
#define LSDPF_constrained_Dijkstra_Visitor_hpp


#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/signals2.hpp>
#include "Instance_constrained.hpp"
#include <vector>
#include "Const.h"
#include "Graph.h"

extern u_int8_t nbTimesTargetReached_constrained;
extern bool allTargetReached_constrained;
extern std::vector<double> worstCriteriaValues_constrained;
extern boost::signals2::signal<void()> onTargetReached_constrained;

class LSDPF_constrained_DijkstraVisitor : public boost::default_bfs_visitor {
private:
    const vertex_t &target;  // the main target to reach
    // the structure containing criteria values between the target and each vertex
    std::vector<std::vector<double>> &criteriaValues;
    
public:
    /**
     * Constructor of a Dijkstra visitor
     * @param _target the target descriptor
     */
    LSDPF_constrained_DijkstraVisitor(const vertex_t &_target, std::vector<std::vector<double>> &_criteriaValues) :
    target(_target), criteriaValues(_criteriaValues) {}
    
    template<typename Vertex, typename Graph>
    void examine_vertex(const Vertex&, Graph&) const {}
    
    template<typename Vertex, typename Graph>
    void finish_vertex(const Vertex &v, Graph&) {
        
        if (v == target) {
            onTargetReached_constrained();
        }
        
        if (allTargetReached_constrained) {
            bool isInteresting = false;
            for (u_int8_t k = 0; k < NB_CRITERIA_COST; ++k) {
                if (criteriaValues[v][k] < worstCriteriaValues_constrained[k]) {
                    isInteresting = true;
                }
            }
            bool exceedsMaxValue = false;
            for (u_int8_t i = 0; i < NB_CRITERIA_CONSTRAINED; i++){
                if ( criteriaValues[v][NB_CRITERIA_COST + i] > maxValuesConstrained[i]){
                    exceedsMaxValue = true;
                    break;
                }
            }
            if (!isInteresting || exceedsMaxValue) {
                throw(0);
            }
        }
    }
    
    template<typename Edge, typename Graph>
    void edge_relaxed(const Edge &e, Graph &g) const {
        // Getting edge criteria values
        const std::vector<double> &edgeCriteriaValues = boost::get(boost::edge_bundle, g)[e].getCriteriaValues();
        
        // Updating criteria values
        for (u_int8_t k = 0; k < NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED; ++k) {
            criteriaValues[boost::target(e, g)][k] =
            criteriaValues[boost::source(e, g)][k] + edgeCriteriaValues[k];
        }
    }
    
    template<typename Edge, typename Graph>
    void edge_not_relaxed(const Edge&, Graph&) const {}
};
#endif /* LSDPF_constrained_Dijkstra_Visitor_hpp */
