# DO NOT EDIT
# This makefile makes sure all linkable targets are
# up-to-date with anything they link to
default:
	echo "Do not invoke directly"

# Rules to remove targets that are older than anything to which they
# link.  This forces Xcode to relink the targets from scratch.  It
# does not seem to check these dependencies itself.
PostBuild.label_setting.Debug:
/Users/arthurcrocquevieille/Documents/Cours/DI5/PRD/label-correcting/_build/Debug/label_setting:\
	/usr/local/lib/libboost_system-mt.dylib\
	/usr/local/lib/libboost_filesystem-mt.dylib\
	/usr/local/lib/libboost_thread-mt.dylib\
	/usr/local/lib/libboost_timer-mt.dylib\
	/usr/local/lib/libboost_signals-mt.dylib\
	/usr/local/lib/libboost_chrono-mt.dylib\
	/usr/local/lib/libboost_date_time-mt.dylib\
	/usr/local/lib/libboost_atomic-mt.dylib
	/bin/rm -f /Users/arthurcrocquevieille/Documents/Cours/DI5/PRD/label-correcting/_build/Debug/label_setting


PostBuild.label_setting.Release:
/Users/arthurcrocquevieille/Documents/Cours/DI5/PRD/label-correcting/_build/Release/label_setting:\
	/usr/local/lib/libboost_system-mt.dylib\
	/usr/local/lib/libboost_filesystem-mt.dylib\
	/usr/local/lib/libboost_thread-mt.dylib\
	/usr/local/lib/libboost_timer-mt.dylib\
	/usr/local/lib/libboost_signals-mt.dylib\
	/usr/local/lib/libboost_chrono-mt.dylib\
	/usr/local/lib/libboost_date_time-mt.dylib\
	/usr/local/lib/libboost_atomic-mt.dylib
	/bin/rm -f /Users/arthurcrocquevieille/Documents/Cours/DI5/PRD/label-correcting/_build/Release/label_setting


PostBuild.label_setting.MinSizeRel:
/Users/arthurcrocquevieille/Documents/Cours/DI5/PRD/label-correcting/_build/MinSizeRel/label_setting:\
	/usr/local/lib/libboost_system-mt.dylib\
	/usr/local/lib/libboost_filesystem-mt.dylib\
	/usr/local/lib/libboost_thread-mt.dylib\
	/usr/local/lib/libboost_timer-mt.dylib\
	/usr/local/lib/libboost_signals-mt.dylib\
	/usr/local/lib/libboost_chrono-mt.dylib\
	/usr/local/lib/libboost_date_time-mt.dylib\
	/usr/local/lib/libboost_atomic-mt.dylib
	/bin/rm -f /Users/arthurcrocquevieille/Documents/Cours/DI5/PRD/label-correcting/_build/MinSizeRel/label_setting


PostBuild.label_setting.RelWithDebInfo:
/Users/arthurcrocquevieille/Documents/Cours/DI5/PRD/label-correcting/_build/RelWithDebInfo/label_setting:\
	/usr/local/lib/libboost_system-mt.dylib\
	/usr/local/lib/libboost_filesystem-mt.dylib\
	/usr/local/lib/libboost_thread-mt.dylib\
	/usr/local/lib/libboost_timer-mt.dylib\
	/usr/local/lib/libboost_signals-mt.dylib\
	/usr/local/lib/libboost_chrono-mt.dylib\
	/usr/local/lib/libboost_date_time-mt.dylib\
	/usr/local/lib/libboost_atomic-mt.dylib
	/bin/rm -f /Users/arthurcrocquevieille/Documents/Cours/DI5/PRD/label-correcting/_build/RelWithDebInfo/label_setting




# For each target create a dummy ruleso the target does not have to exist
/usr/local/lib/libboost_atomic-mt.dylib:
/usr/local/lib/libboost_chrono-mt.dylib:
/usr/local/lib/libboost_date_time-mt.dylib:
/usr/local/lib/libboost_filesystem-mt.dylib:
/usr/local/lib/libboost_signals-mt.dylib:
/usr/local/lib/libboost_system-mt.dylib:
/usr/local/lib/libboost_thread-mt.dylib:
/usr/local/lib/libboost_timer-mt.dylib:
