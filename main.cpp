/*
 * Copyright 2017 Giret
 * */

#include <ostream>
#include <string>
#include <vector>
#include <map>
#include "include/Parser.h"
#include "include/Graph.h"
#include "include/LSDPF.h"
#include "include/Writer.h"

namespace {
    std::map<u_int8_t, u_int8_t> weightsMap_impl;

    bool initializeConst() {
        for (u_int8_t n = 0; n < firstPhaseWeights.size(); ++n) {
            for (u_int8_t k = 0; k < NB_CRITERIA_COST+ NB_CRITERIA_CONSTRAINED + NB_CRITERIA_MIN_MAX; ++k) {
                if (firstPhaseWeights[n][k] == 1.) {
                    weightsMap_impl[k] = n;
                    break;
                }
            }
        }

        return true;
    }
}  // namespace

const std::map<u_int8_t, u_int8_t> &weightsMap = weightsMap_impl;

int main(int argc, char **argv) {
    try {
        if (argc < 2) {
            throw std::invalid_argument("Vertices file path not provided");
        } else if (argc < 3) {
            throw std::invalid_argument("Edges file path not provided");
        } else if (argc < 4) {
            throw std::invalid_argument("Instances file path not provided");
        } else if (argc < 5) {
            throw std::invalid_argument("Results directory path not provided");
        }

        initializeConst();

        //Change parser
        auto *parser = new Parser();
        parser->parseVerticesFile(std::string(argv[1]));
        parser->parseEdgesFile(std::string(argv[2]));
        parser->parseInstancesFile(std::string(argv[3]));

        auto *graph = new Graph();
        graph->setVertices(parser->getVertices());
        graph->setEdges(parser->getEdges());
        graph->printData();

        auto *writer = new Writer(argv[4]);

        for (const Instance &instance : parser->getInstances()) {
            
            auto *lsdpf = new LSDPF(graph, instance.getSource(), instance.getTarget(), instance.getMaxValuesConstrained());
            lsdpf->runFirstPhase();
            lsdpf->runSecondPhase();
            lsdpf->printStatistics(instance.getId());
            writer->writeResults(instance, lsdpf);
            writer->writeParetoFrontFile(instance, lsdpf);

            delete lsdpf;
        }

        delete writer;
        delete graph;
        delete parser;
    } catch (const std::exception &e) {
        std::cerr << "Exception caught: " << e.what() << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
