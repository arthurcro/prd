/*
 * Copyright 2017 Giret
 * */

#ifndef INCLUDE_PARSER_H_
#define INCLUDE_PARSER_H_

#include <vector>
#include <string>
#include <fstream>
#include "Const.h"
#include "Vertex.h"
#include "Edge.h"
#include "Instance.h"

class Parser {
 private:
    std::vector<Vertex> vertices;  // the vertices list
    std::vector<Edge> edges;  // the edges list
    std::vector<Instance> instances;  // the instances list

 public:
    /**
     * Constructor of a parser object
     */
    Parser() {}

    /**
     * Destructor of a parser object
     */
    ~Parser();

    /**
     * Getter of the vertices list
     * @return the vertices list
     */
    std::vector<Vertex> getVertices() {
        return vertices;
    }

    /**
     * Getter of the edges list
     * @return the edges list
     */
    std::vector<Edge> getEdges() {
        return edges;
    }

    /**
     * Getter of the instances list
     * @return the instances list
     */
    const std::vector<Instance> &getInstances() const {
        return instances;
    }

    /**
     * Method to parse a file containing the vertices data
     * @param verticesFilePath the path of the file containing all vertices
     */
    void parseVerticesFile(const std::string &verticesFilePath);

    /**
     * Method to parse a file containing the edges data
     * @param edgesFilePath the path of the file containing all edges
     */
    void parseEdgesFile(const std::string &edgesFilePath);

    /**
     * Method to parse a file containing the instances data
     * @param instancesFilePath the path of the file containing all instances
     */
    void parseInstancesFile(const std::string &instancesFilePath);
};


#endif  // INCLUDE_PARSER_H_
