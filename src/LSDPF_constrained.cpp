//
//  LSDPF_constrained.cpp
//  label_setting
//
//  Created by Arthur Crocquevieille on 10/01/2018.
//

#include "../include/LSDPF_constrained.h"
#include "../include/Instance_constrained.hpp"

LSDPF_constrained::LSDPF_constrained(const Graph *_graph, const int64_t _sourceId, const int64_t _targetId) :
criteriaValues(std::vector<std::vector<std::vector<double>>>(firstPhaseWeights.size(),std::vector<std::vector<double>>(num_vertices(_graph->getInstance()),std::vector<double>(NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED,std::numeric_limits<double>::max())))),vertexLabelList(new vertex_label_list_t[_graph->getInstance().m_vertices.size()]),graph(_graph->getInstance()), source(_graph->getVertex(_sourceId)),labelId(-1), target(_graph->getVertex(_targetId))

{
    
    /*
     * Creates a slot which will be called when the target is reached
     * it allows to retrieve the worst value of each criteria
     * it's also used to indicate the end of the first preprocessing phase (allTargetReached)
     * The second processing phase 
     */
    onTargetReached_constrained.connect([this] {
        ++nbTimesTargetReached_constrained;
        if (nbTimesTargetReached_constrained == firstPhaseWeights.size()) {
            for (u_int8_t n = 0; n < firstPhaseWeights.size(); ++n) {
                
                for (u_int8_t k = 0; k < NB_CRITERIA_COST; ++k) {
                    worstCriteriaValues_constrained[k] = std::max(worstCriteriaValues_constrained[k], criteriaValues[n][source][k]);
                }
                for( u_int8_t i = 0; i < NB_CRITERIA_CONSTRAINED; i++){
                    //Worst for constrained criteria = max value constrained
                    worstCriteriaValues_constrained[NB_CRITERIA_COST + i] = maxValuesConstrained[i];
                }

            }
            
            allTargetReached_constrained = true;
            onTargetReached_constrained.disconnect_all_slots();
        }
    });
}

LSDPF_constrained::~LSDPF_constrained() {
    criteriaValues.clear();
    delete[] vertexLabelList;
    nbTimesTargetReached_constrained = 0;
    allTargetReached_constrained = false;
    worstCriteriaValues_constrained = std::vector<double>(NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED, 0.);
}

void LSDPF_constrained::runFirstPhase() {
    
    //Start the timer
    boost::timer::cpu_timer firstPhaseTimer;
    firstPhaseTimer.start();
    //Reversing the graph
    boost::reverse_graph<graph_t> reversedInstance = boost::make_reverse_graph(graph);
    
    //Running reverse Dijkstra searchs in multiple threads
    boost::thread_group threadGroup;
    for (uint8_t n = 0; n < firstPhaseWeights.size(); ++n){
        threadGroup.create_thread(boost::bind(&LSDPF_constrained::runReversedDijkstra, this, boost::ref(reversedInstance),n));
    }
    threadGroup.join_all();
    
    /*
     * TODO : For each vertext :
     *  Remove it from the graph if it has a constrained value > maxConstrainedValue
     **/
    
    //Stop the timer
    firstPhaseTimer.stop();
    stats.firstPhaseTime = getElapsedTime(firstPhaseTimer);
    
}

void LSDPF_constrained::runReversedDijkstra(const boost::reverse_graph<graph_t> &reverseGraph, const u_int8_t n) {
    using boost::dijkstra_shortest_paths;
    using boost::make_iterator_property_map;
    using boost::make_transform_value_property_map;
    
    // Initializing boost graph parameters
    std::vector<double> distances(num_vertices(graph));
    std::vector<int64_t> predecessors(num_vertices(graph));
    
    auto predecessorMap = make_iterator_property_map(predecessors.begin(), get(&Vertex::id, reverseGraph));
    auto distanceMap = make_iterator_property_map(distances.begin(), get(&Vertex::id, reverseGraph));
    auto weightMap = make_transform_value_property_map([_n = n](const Edge &e) -> double {
        // Getting edge linear combination value according to a set of defined weights
        double lcValue = 0.;
        for (u_int8_t k = 0; k < NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED; ++k) {
            lcValue += e.getCriteriaValues()[k] * firstPhaseWeights[_n][k];
        }
        return lcValue;
    }, boost::get(boost::edge_bundle, reverseGraph));
    
    // Initializing criteria values
    criteriaValues[n][target] = std::vector<double>(NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED, 0.);
    
    // Running the reversed Dijkstra one-to-all search
    try {
        dijkstra_shortest_paths(reverseGraph,
                                target,
                                predecessor_map(predecessorMap)
                                .distance_map(distanceMap)
                                .weight_map(weightMap)
                                .vertex_index_map(boost::get(&Vertex::id, reverseGraph))
                                .visitor(LSDPF_constrained_DijkstraVisitor(source, criteriaValues[n])));
    } catch (int) {}  // Prevent Dijkstra cut
}

int64_t LSDPF_constrained::getNextLabelId() {
    ++labelId;
    if (labelId == std::numeric_limits<int64_t>::max()) {
        throw std::invalid_argument("The number of created labels is too high");
    }
    return labelId;
}

template<typename Tag>
Label LSDPF_constrained::getNextLabel() {
    auto nextLabelIt = explorationQueue.get<Tag>().begin();
    Label label = *nextLabelIt;
    explorationQueue.get<Tag>().erase(nextLabelIt);
    return label;
}
void LSDPF_constrained::runSecondPhase(){
    //Start timer
    boost::timer::cpu_timer secondPhaseTimer;
    secondPhaseTimer.start();
    
    /*
     * Building the initial label
     * Adding initial label to the exploration queue
     * Adding initial label to source vertex label list
     */
    Label initialLabel(getNextLabelId(),
                       source,
                       -1,
                       std::vector<double>(NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED, 0.),
                       std::vector<double>(NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED, 0.));
    //Add an initial label to the source vertex
    addLabelToVertex(initialLabel);
    explorationQueue.insert(initialLabel);
    
    //Initialize the pareto front
    for(u_int8_t n = 0; n < firstPhaseWeights.size(); n++){
        //Retrieve the values obtained but the first phase by the n-th search
        const std::vector<double> &values = criteriaValues[n][source];
        // Testing if initial label will not be dominated by other ones
        if (!isDominated(values, target)) {
            // Adding initial label to the Pareto front
            addLabelToVertex(Label(getNextLabelId(), target, initialLabel.getId(), values));
        }
    }
    
    //Running label-setting while exploration queue is not empty
    while(!explorationQueue.empty()){
        // Selecting the next label to propagate
        Label currentLabel = getNextLabel<Label::ByProjectedCriteriaValues>();
        
        // Getting the current label criteria values
        const std::vector<double> &currentCriteriaValues = currentLabel.getCriteriaValues();
        
        out_edge_iterator_t edgeIt, outEdgesEnd;
        tie(edgeIt, outEdgesEnd) = out_edges(currentLabel.getVertex(), graph);
        for (; edgeIt != outEdgesEnd; ++edgeIt) {
            // Getting the criteria values of the edge
            const std::vector<double> &edgeCriteriaValues = graph[*edgeIt].getCriteriaValues();
            
            // Getting the current vertex
            const vertex_t &currentVertex = boost::target(*edgeIt, graph);
            
            // Initializing criteria values and projected criteria values of the label
            std::vector<double> values(NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED);
            std::vector<double> projectedValues(NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED, std::numeric_limits<double>::max());
            for (u_int8_t k = 0; k <NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED; ++k) {
                // Getting index of Dijkstra search considering only this criteria
                const u_int8_t n = weightsMap.find(k)->second;
                
                // Getting the criteria values of the new label
                const double &newValue = values[k] = currentCriteriaValues[k] + edgeCriteriaValues[k];
                
                // Getting the projected criteria values of the new label
                const double &projectedValue = criteriaValues[n][currentVertex][k];
                if (projectedValue != std::numeric_limits<double>::max()) {
                    projectedValues[k] = newValue + projectedValue;
                }
            
                // Testing if new label will not be dominated on Pareto front
                //projected values vs target = IS_PROMISING
                if (currentVertex == target || !isDominated(projectedValues, target)){
                    //Testing if new label will not be dominated on current vertex
                    //Testing if new label exceeds one of the  max values for the constrained ressources
                    bool exceedsRessource = false;
                    for ( u_int8_t i = 0; i < NB_CRITERIA_CONSTRAINED ; i++){
                        if (values[NB_CRITERIA_COST + i] > maxValuesConstrained[i]){
                            exceedsRessource = true;
                            break;
                        }
                    }
                    if(!isDominated(values,currentVertex) && !exceedsRessource){
                        // Building the new label
                        Label newLabel(getNextLabelId(),
                                       currentVertex,
                                       currentLabel.getId(),
                                       values,
                                       projectedValues);
                        
                        // Adding new label to current vertex
                        addLabelToVertex(newLabel);
                        // Purging dominated labels of current vertex
                        {
                            std::vector<Label> dominatedLabels;
                            purgeDominated(newLabel, dominatedLabels);
                            auto dominatedLabelIt = dominatedLabels.begin();
                            for (; dominatedLabelIt != dominatedLabels.end(); ++dominatedLabelIt) {
                                const auto &labelList = explorationQueue.get<Label::ById>();
                                auto labelIt = labelList.find(dominatedLabelIt->getId());
                                if (labelIt != labelList.end()) {
                                    explorationQueue.get<Label::ById>().erase(labelIt);
                                }
                            }
                            dominatedLabels.clear();
                        }
                        // Updating ynamically the Pareto front
                        for (u_int8_t n : secondPhaseWeightIndexes) {
                            // Initializing criteria values of the projected label
                            std::vector<double> projectedLabelValues(NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED, std::numeric_limits<double>::max());
                            for (u_int8_t k = 0; k < NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED; ++k) {
                                const double &projectedValue = criteriaValues[n][currentVertex][k];
                                if (projectedValue != std::numeric_limits<double>::max()) {
                                    projectedLabelValues[k] = values[k] + projectedValue;
                                }
                            }
                            
                            // Testing if projected label will not be dominated on Pareto front
                            if (!isDominated(projectedLabelValues, target)) {
                                // Building projected label
                                const Label projectedLabel(getNextLabelId(),
                                                           target,
                                                           newLabel.getId(),
                                                           projectedLabelValues);
                                
                                // Adding projected label to Pareto front
                                const auto &labelList = vertexLabelList[target].get<Label::ByCriteriaValues>();
                                if (labelList.find(projectedLabelValues) == labelList.end()) {
                                    vertexLabelList[target].insert(projectedLabel);
                                    ++stats.nbLabelsAddedToPF;
                                    
                                    // Purging dominated labels on Pareto front
                                    std::vector<Label> dominatedLabels;
                                    purgeDominated(projectedLabel, dominatedLabels);
                                    dominatedLabels.clear();
                                }
                            }
                        }
                        // Adding new label to exploration queue if target not reached
                        if (currentVertex != target) {
                            explorationQueue.insert(newLabel);
                        }
                    }
                }
            }
        }
        
        
    }
    //Stop timer
    secondPhaseTimer.stop();
    stats.secondPhaseTime = getElapsedTime(secondPhaseTimer);
}

bool LSDPF_constrained::isDominated(const std::vector<double> &criteriaValues, const vertex_t vertex) const {
    // Getting labels of the vertex
    const auto &labelList = vertexLabelList[vertex].get<Label::ByCriteriaValues>();
    
    // Testing if the set of criteria values is dominated by a label
    auto labelListIt = labelList.upper_bound(criteriaValues);
    if (labelListIt == labelList.begin()) {
        return false;
    } else {
        --labelListIt;
        const std::vector<double> &labelCriteriaValues = labelListIt->getCriteriaValues();
        bool isDominated = true;
        //If we test domination on the target then we only care about the first 2 criteria
        //Otherwise we care about all of them, even the constrained ressources
        u_int8_t nbCriteria = (vertex == target) ? NB_CRITERIA_COST : (NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED);
        for (u_int8_t k = 0; k < nbCriteria; ++k) {
            isDominated = isDominated && criteriaValues[k] >= labelCriteriaValues[k];
        }
        return isDominated;
    }
}



void LSDPF_constrained::purgeDominated(const Label &label, std::vector<Label> &dominatedLabels) {
    // Getting labels of the vertex
    const auto &labelList = vertexLabelList[label.getVertex()].get<Label::ByCriteriaValues>();
    
    // Getting dominated labels
    const std::vector<double> &criteriaValues = label.getCriteriaValues();
    auto labelListIt = ++labelList.find(criteriaValues);
    while (labelListIt != labelList.end()) {
        const std::vector<double> &labelCriteriaValues = labelListIt->getCriteriaValues();
        bool isDominated = true;
        
        //If we test domination on the target then we only care about the first 2 criteria
        //Otherwise we care about all of them, even the constrained ressources
        u_int8_t nbCriteria = (label.getVertex() == target) ? NB_CRITERIA_COST : (NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED);
        for (u_int8_t k = 0; k < nbCriteria ; ++k) {
            isDominated = isDominated && labelCriteriaValues[k] >= criteriaValues[k];
        }
        
        if (isDominated) {
            dominatedLabels.push_back(*labelListIt);
            labelListIt = vertexLabelList[label.getVertex()].get<Label::ByCriteriaValues>().erase(labelListIt);
        } else {
            ++labelListIt;
        }
    }
}
void LSDPF_constrained::addLabelToVertex(const Label &label){
    //Get the identifier of the vertex where to attach the new label
    const vertex_t vertex = label.getVertex();
    //Get the labels already attached to the vertex ordered by criteria values
    const auto &labelList = vertexLabelList[vertex].get<Label::ByCriteriaValues>();
    
    /*
     * Testing if a label with similar criteria values already exists
     * If it doesn't exist, add the new label
     * Else replace it with the new label
     */
    auto labelListIt = labelList.find(label.getCriteriaValues());
    if (labelListIt == labelList.end()) {
        vertexLabelList[vertex].insert(label);
        if (vertex == target) {
            ++stats.nbLabelsAddedToPF;
        }
    } else {
        vertexLabelList[vertex].replace(labelListIt, label);
    }
}


unsigned int LSDPF_constrained::getElapsedTime(const boost::timer::cpu_timer &timer) const {
    return static_cast<unsigned int>(timer.elapsed().system + timer.elapsed().user) / 1000000;
}

void LSDPF_constrained::printStatistics() const {
    std::cout << "LSDPF statistics:" << std::endl
    // << "Solutions: " << getParetoFront().size() << std::endl
    << "First phase time: " << stats.firstPhaseTime << "ms" << std::endl
    << "Second phase time: " << stats.secondPhaseTime << "ms" << std::endl
    << "Created labels: " << stats.nbCreatedLabels << std::endl
    << "Labels added to PF: " << stats.nbLabelsAddedToPF << std::endl
    << std::flush;
}

void LSDPF_constrained::printStatistics(const int64_t instanceId) const {
    const unsigned int elapsedTime = stats.firstPhaseTime + stats.secondPhaseTime;
    
    std::cout << "#" << instanceId << ":\t"
    //<< "|S| = " << getParetoFront().size() << ",\t"
    << "time = " << elapsedTime << "ms"
    << std::endl << std::flush;
}
