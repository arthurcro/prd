
//
//  LSDPF_constrained_Dijkstra_Visitor.cpp
//  label_setting
//
//  Created by Arthur Crocquevieille on 18/01/2018.
//

#include "../include/LSDPF_constrained_Dijkstra_Visitor.hpp"

u_int8_t nbTimesTargetReached_constrained = 0;
bool allTargetReached_constrained = false;
std::vector<double> worstCriteriaValues_constrained = std::vector<double>(NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED, 0.);

boost::signals2::signal<void()> onTargetReached_constrained;
