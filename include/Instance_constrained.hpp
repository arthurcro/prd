//
//  Instance_constrained.hpp
//  label_setting
//
//  Created by Arthur Crocquevieille on 10/01/2018.
//

#ifndef Instance_constrained_hpp
#define Instance_constrained_hpp

#include "Const.h"
#include <cstdint>
#include <vector>

extern std::vector<u_int64_t> maxValuesConstrained; // the value maximum of the constrained ressources

class Instance_constrained {
    
private:
    const int64_t id;  // the identifier of the instance
    const int64_t source;  // the source vertex identifier of the instance
    const int64_t target;  // the target vertex identifier of the instance
    
public:
    /**
     * Constructor of an instance object
     * @param _id the identifier of the instance
     * @param _source the source vertex identifier of the instance
     * @param _target the target vertex identifier of the instance
     */
    Instance_constrained(const int64_t _id, const int64_t _source, const int64_t _target, const std::vector<u_int64_t> _maxValuesConstrained) :
    id(_id), source(_source), target(_target) {
        maxValuesConstrained = _maxValuesConstrained;
    }
    
    /**
     * Getter of the identifier of the instance
     * @return the identifier of the instance
     */
    int64_t getId() const {
        return id;
    }
    
    /**
     * Getter of the source vertex identifier of the instance
     * @return the source vertex identifier of the instance
     */
    int64_t getSource() const {
        return source;
    }
    
    /**
     * Getter of the target vertex identifier of the instance
     * @return the target vertex identifier of the instance
     */
    int64_t getTarget() const {
        return target;
    }
    
};

#endif /* Instance_constrained_hpp */
