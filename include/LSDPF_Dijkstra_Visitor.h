/*
 * Copyright 2017 Giret
 * */

#ifndef INCLUDE_LSDPF_DIJKSTRA_VISITOR_H_
#define INCLUDE_LSDPF_DIJKSTRA_VISITOR_H_

#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/signals2.hpp>
#include "Instance.h"
#include <vector>
#include "Const.h"
#include "Graph.h"

extern u_int8_t nbTimesTargetReached;
extern bool allTargetReached;
extern std::vector<double> worstCriteriaValues;
extern boost::signals2::signal<void()> onTargetReached;

class LSDPFDijkstraVisitor : public boost::default_bfs_visitor {
 private:
    
    std::vector<int> maxValuesConstrained;
    const vertex_t &target;  // the main target to reach
    // the structure containing criteria values between the target and each vertex
    std::vector<std::vector<double>> &criteriaValues;

 public:
    /**
     * Constructor of a Dijkstra visitor
     * @param _target the target descriptor
     */
    LSDPFDijkstraVisitor(const vertex_t &_target, std::vector<std::vector<double>> &_criteriaValues, std::vector<int> _maxValuesConstrained) :
            target(_target), criteriaValues(_criteriaValues) {
                maxValuesConstrained = _maxValuesConstrained;
             }

    template<typename Vertex, typename Graph>
    void examine_vertex(const Vertex&, Graph&) const {}

    template<typename Vertex, typename Graph>
    void finish_vertex(const Vertex &v, Graph&) {
        
        if (v == target) {
            onTargetReached();
        }

        if (allTargetReached) {
            
            bool isInteresting = false;
            bool exceedsMaxValue = false;
            bool isInterestingMinMax = false;
            
            //Cost
            for (u_int8_t k = 0; k < NB_CRITERIA_COST; ++k) {
                if (criteriaValues[v][k] < worstCriteriaValues[k]) {
                    isInteresting = true;
                }
            }

            //Resources
            for (u_int8_t i = 0; i < NB_CRITERIA_CONSTRAINED; i++){
                if ( criteriaValues[v][NB_CRITERIA_COST + i] > maxValuesConstrained[i]){
                    exceedsMaxValue = true;
                    break;
                }
            }
            
            //Max min
            for (u_int8_t i = 0; i < NB_CRITERIA_MIN_MAX; i++){
                if (criteriaValues[v][NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED + i] < worstCriteriaValues[NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED + i]) {
                    isInterestingMinMax = true;
                }
            }
            
            if ((!isInteresting && !isInterestingMinMax) || exceedsMaxValue) {
                throw(0);
            }
        }
    }

    template<typename Edge, typename Graph>
    void edge_relaxed(const Edge &e, Graph &g) const {
        // Getting edge criteria values
        const std::vector<double> &edgeCriteriaValues = boost::get(boost::edge_bundle, g)[e].getCriteriaValues();

        // Updating criteria values +
        for (u_int8_t k = 0; k < NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED; ++k) {
            criteriaValues[boost::target(e, g)][k] =
                    criteriaValues[boost::source(e, g)][k] + edgeCriteriaValues[k];
        }

        for ( u_int8_t k = 0 ; k < NB_CRITERIA_MIN_MAX ; k++){
            criteriaValues[boost::target(e, g)][NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED + k]  = std::max(criteriaValues[boost::source(e, g)][NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED + k], edgeCriteriaValues[NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED + k]);
        }
    }

    template<typename Edge, typename Graph>
    void edge_not_relaxed(const Edge&, Graph&) const {}
};

#endif  // INCLUDE_LSDPF_DIJKSTRA_VISITOR_H_
