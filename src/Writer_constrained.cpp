//
//  Writer_constrained.cpp
//  label_setting
//
//  Created by Arthur Crocquevieille on 23/01/2018.
//

#include "../include/Writer_constrained.hpp"

Writer_constrained::Writer_constrained(const std::string &_resultFilesDir) :
resultsFilesDir(_resultFilesDir + "/" + getCurrentTimeFormatted()) {
    // Creating repository in which to write results
    boost::filesystem::path dir(resultsFilesDir.c_str());
    if (create_directory(dir)) {
        // Creating result file
        std::string resultFilePath = resultsFilesDir + "/results.csv";
        resultsFile.open(resultFilePath);
        
        // Writing first phase weights
        resultsFile << "First phase weights;";
        for (const auto &firstPhaseWeight : firstPhaseWeights) {
            resultsFile << "{";
            for (u_int8_t k = 0; k < NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED; ++k) {
                resultsFile << firstPhaseWeight[k];
                
                if (k != (NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED) - 1) {
                    resultsFile << " ";
                }
            }
            resultsFile << "};";
        }
        resultsFile << std::endl;
        
        // Writing second phase weights
        resultsFile << "Second phase weights;";
        for (const u_int8_t secondPhaseWeightIndex : secondPhaseWeightIndexes) {
            resultsFile << "{";
            for (u_int8_t k = 0; k < NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED; ++k) {
                resultsFile << firstPhaseWeights[secondPhaseWeightIndex][k];
                
                if (k != (NB_CRITERIA + NB_CRITERIA_CONSTRAINED) - 1) {
                    resultsFile << " ";
                }
            }
            resultsFile << "};";
        }
        resultsFile << std::endl;
        
        // Writing label selection strategy
        resultsFile << "Label selection strategy;A*" << std::endl;
        
        // Writing headers
        resultsFile << std::endl
        << "#;Solutions;First phase (ms);Second phase (ms);Labels created;Labels added to PF"
        << std::endl;
    } else {
        throw std::invalid_argument("Result directory cannot be created");
    }
}

Writer_constrained::~Writer_constrained() {
    resultsFile.close();
}

const std::string Writer_constrained::getCurrentTimeFormatted() {
    time_t t = time(nullptr);
    struct tm tp{};
    struct tm *now = localtime_r(&t, &tp);
    std::stringstream currentTime;
    int year = now->tm_year + 1900,
    month = now->tm_mon + 1,
    day = now->tm_mday,
    hours = now->tm_hour,
    minutes = now->tm_min,
    seconds = now->tm_sec;
    
    currentTime << year
    << (month < 10 ? "0" : "") << month
    << (day < 10 ? "0" : "") << day
    << (hours < 10 ? "0" : "") << hours
    << (minutes < 10 ? "0" : "") << minutes
    << (seconds < 10 ? "0" : "") << seconds;
    
    return currentTime.str();
}

void Writer_constrained::writeResults(const Instance_constrained &instance, const LSDPF_constrained *lsdpf) {
    const Stats_constrained &stats = lsdpf->getStats();
    
    resultsFile << instance.getId() << ";"
    << lsdpf->getParetoFront().size() << ";"
    << stats.firstPhaseTime << ";"
    << stats.secondPhaseTime << ";"
    << stats.nbCreatedLabels << ";"
    << stats.nbLabelsAddedToPF
    << std::endl;
}

void Writer_constrained::writeParetoFrontFile(const Instance_constrained &instance, const LSDPF_constrained *lsdpf) {
    // Creating Pareto front file
    std::string paretoFrontFilePath = resultsFilesDir + "/results_" +
    (instance.getId() < 10 ? "0" : "") + std::to_string(instance.getId()) + ".csv";
    std::ofstream paretoFrontFile(paretoFrontFilePath);
    
    const auto &labelList = lsdpf->getParetoFront().get<Label::ByCriteriaValues>();
    for (const auto &labelListIt : labelList) {
        const std::vector<double> &criteriaValues = labelListIt.getCriteriaValues();
        for (u_int8_t k = 0; k < NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED; ++k) {
            paretoFrontFile << criteriaValues[k] << ";";
        }
        paretoFrontFile << std::endl;
    }
    
    paretoFrontFile.close();
}
