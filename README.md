### Install dependencies
```bash
sudo apt install libboost-dev zlib1g-dev
```

```bash
cd /opt
git clone https://github.com/osmcode/libosmium.git
```

### Build
```bash
mkdir build
cd build/
cmake ..
make
/Users/arthurcrocquevieille/Documents/Cours/DI5/PRD/label-correcting/cmake/FindOsmium.cmake```
