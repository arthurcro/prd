/*
 * Copyright 2017 Giret
 * */

#ifndef INCLUDE_EDGE_H_
#define INCLUDE_EDGE_H_

#include <utility>
#include <vector>

class Edge {
 private:
    const int64_t id;  // the identifier of the edge
    const int64_t source;  // the source vertex identifier of the edge
    const int64_t target;  // the target vertex identifier of the edge
    const std::vector<double> criteriaValues;  // the criteria values of the edge

 public:
    /**
     * Constructor of an edge object
     * @param _id the identifier of the edge
     * @param _source the source vertex identifier of the edge
     * @param _target the target vertex identifier of the edge
     * @param _criteriaValues the criteria values of the edge
     */
    Edge(const int64_t _id, const int64_t _source, const int64_t _target, std::vector<double> _criteriaValues) :
            id(_id), source(_source), target(_target), criteriaValues(std::move(_criteriaValues)) {}

    /**
     * Getter of the source vertex identifier of the edge
     * @return the source vertex identifier
     */
    int64_t getSource() {
        return source;
    }

    /**
     * Getter of the target vertex identifier of the edge
     * @return the target vertex identifier of the edge
     */
    int64_t getTarget() {
        return target;
    }

    /**
     * Getter of the edge criteria values
     * @return the criteria values of the edge
     */
    const std::vector<double> &getCriteriaValues() const {
        return criteriaValues;
    }
};


#endif  // INCLUDE_EDGE_H_
