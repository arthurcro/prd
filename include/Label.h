/**
 * Copyright 2017 Giret
 */

#ifndef INCLUDE_LABEL_H_
#define INCLUDE_LABEL_H_

#include <utility>
#include <vector>
#include <limits>

class Label {
 private:
    int64_t id;  // the identifier of the label
    vertex_t vertex;  // the vertex where the label is attached
    int64_t parentId;  // the parent label identifier of the label
    std::vector<double> criteriaValues;  // the criteria values of the label
    std::vector<double> projectedCriteriaValues;  // the projected criteria values of the label

 public:
    
    struct ById {};
    struct ByCriteriaValues {};
    struct ByProjectedCriteriaValues {};

    /**
     * Constructor of a label object
     * @param _id the identifier of the label
     * @param _vertex the vertex where the label is attached
     * @param _parentId the parent label identifier of the label
     * @param _criteriaValues the criteria values of the label
     * @param _projectedCriteriaValues the projected criteria values of the label
     */
    Label(const int64_t _id,
          const vertex_t _vertex,
          const int64_t _parentId,
          std::vector<double> _criteriaValues,
          std::vector<double> _projectedCriteriaValues) :
            id(_id),
            vertex(_vertex),
            parentId(_parentId),
            criteriaValues(std::move(_criteriaValues)),
            projectedCriteriaValues(std::move(_projectedCriteriaValues)) {}

    /**
     * Constructor of a label object
     * @param _id the identifier of the label
     * @param _vertex the vertex where the label is attached
     * @param _parentId the parent label identifier of the label
     * @param _criteriaValues the criteria values of the label
     */
    Label(const int64_t _id,
          const vertex_t _vertex,
          const int64_t _parentId,
          std::vector<double> _criteriaValues) :
            id(_id),
            vertex(_vertex),
            parentId(_parentId),
            criteriaValues(std::move(_criteriaValues)),
            projectedCriteriaValues(std::vector<double>(NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED)) {}

    Label(const Label &l) :
            id(l.id),
            vertex(l.vertex),
            parentId(l.parentId),
            criteriaValues(l.criteriaValues),
            projectedCriteriaValues(l.projectedCriteriaValues) {}

    /**
     * Getter of the identifier of the label
     * @return the identifier of the label
     */
    int64_t getId() const {
        return id;
    }

    /**
     * Getter of the vertex where the label is attached
     * @return the vertex where the label is attached
     */
    vertex_t getVertex() const {
        return vertex;
    }

    /**
     * Getter of the parent label identifier of the label
     * @return the parent label identifier of the label
     */
    int64_t getParentId() const {
        return parentId;
    }

    /**
     * Getter of the criteria values of the label
     * @return the criteria values of the label
     */
    const std::vector<double> &getCriteriaValues() const {
        return criteriaValues;
    }

    /**
     * Getter of the projected criteria values of the label
     * @return the projected criteria values of the label
     */
    const std::vector<double> &getProjectedCriteriaValues() const {
        return projectedCriteriaValues;
    }
};

#endif  // INCLUDE_LABEL_H_
