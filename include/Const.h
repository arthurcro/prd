/*
 * Copyright 2017 Giret
 * */

#ifndef INCLUDE_CONST_H_
#define INCLUDE_CONST_H_

#include <vector>
#include <map>

#define NB_CRITERIA_CONSTRAINED 0
#define NB_CRITERIA_COST 2
#define NB_CRITERIA_MIN_MAX 2

const std::vector<std::vector<double>> firstPhaseWeights = {
    
   /*{1., 0.},
    {0., 1.}*/
    {1., 0., 0., 0.}, // premier critère de cout
    {0., 1., 0., 0.},
    {0., 0., 1., 0.},
    {0., 0., 0., 1.}
};

const std::vector<u_int8_t> secondPhaseWeightIndexes = {0, 1, 2, 3};

extern const std::map<u_int8_t, u_int8_t> &weightsMap;


#endif  // INCLUDE_CONST_H_
