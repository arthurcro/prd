/*
 * Copyright 2017 Giret
 * */

#include "../include/Graph.h"

Graph::~Graph() {
    instance.clear();
    verticesMap.clear();
}

void Graph::setVertices(std::vector<Vertex> vertices) {
    for (Vertex vertex : vertices) {
        /*
         * Adding vertices to the graph
         * Associating each vertex identifier to a vertex descriptor
         */
        verticesMap.emplace(std::make_pair(vertex.id, add_vertex(vertex, instance)));
    }
}

void Graph::setEdges(std::vector<Edge> edges) {
    for (Edge edge : edges) {
        // Getting source and target descriptors of the edge
        const vertex_t &source = verticesMap.find(edge.getSource())->second,
                &target = verticesMap.find(edge.getTarget())->second;

        // Adding edge between source and target to the graph
        add_edge(source, target, edge, instance);
    }
}

void Graph::printData() const {
    std::cout << "Graph data:" << std::endl
              << "Vertices: " << num_vertices(instance) << std::endl
              << "Edges: " << num_edges(instance) << std::endl
              << std::flush;
}
