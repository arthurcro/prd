/*
 * Copyright 2017 Giret
 * */

#include "../include/Parser.h"
#include <ostream>


Parser::~Parser() {
    vertices.clear();
    edges.clear();
    instances.clear();
}

void Parser::parseVerticesFile(const std::string &verticesFilePath) {
    int64_t id = 0;
    double lat = 0., lon = 0.;

    // Getting vertices file stream
    std::ifstream verticesFileStream(verticesFilePath);
    if (verticesFileStream.fail()) {
        throw std::invalid_argument("Vertices file not found");
    }

    // Parsing the vertices file
    while (!verticesFileStream.eof()) {
        // Getting vertex data
        verticesFileStream >> id >> lat >> lon;

        // Adding vertex to the vertices list
        vertices.emplace_back(id, osmium::Location(lon, lat));
    }

    vertices.pop_back();

    // Closing the vertices file stream
    verticesFileStream.close();
}

void Parser::parseEdgesFile(const std::string &edgesFilePath) {
    int64_t id = 0, source = 0, target = 0;
    std::vector<double> criteriaValues(NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED + NB_CRITERIA_MIN_MAX, 0.);

    // Getting edges file stream
    std::ifstream edgesFileStream(edgesFilePath);
    if (edgesFileStream.fail()) {
        throw std::invalid_argument("Edges file not found");
    }

    // Parsing the edges file
    while (!edgesFileStream.eof()) {
        // Getting edge data
        edgesFileStream >> source >> target;
        for (u_int8_t k = 0; k < NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED + NB_CRITERIA_MIN_MAX; ++k) {
            edgesFileStream >> criteriaValues[k];
        }

        // Adding edge to the edges list
        edges.emplace_back(id++, source, target, criteriaValues);
    }

    edges.pop_back();

    // Closing the edges file stream
    edgesFileStream.close();
}

void Parser::parseInstancesFile(const std::string &instancesFilePath) {
    int64_t id = 0, source = 0, target = 0;

    // Getting instances file stream
    std::ifstream instancesFileStream(instancesFilePath);
    if (instancesFileStream.fail()) {
        throw std::invalid_argument("Instance file not found");
    }

    // Parsing the instances file
    while (!instancesFileStream.eof()) {
        std::vector<int> maxValuesConstrained (NB_CRITERIA_CONSTRAINED,0);
        // Getting instance data
        instancesFileStream >> id >> source >> target;
        for ( u_int64_t i = 0; i < NB_CRITERIA_CONSTRAINED ; i++){
            instancesFileStream >> maxValuesConstrained[i];
        }
        // Adding instance to the instances list
        instances.emplace_back(id++, source, target, maxValuesConstrained);
    }

    instances.pop_back();

    // Closing the instances file stream
    instancesFileStream.close();
}
