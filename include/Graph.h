/*
 * Copyright 2017 Giret
 * */

#ifndef INCLUDE_GRAPH_H_
#define INCLUDE_GRAPH_H_

#include <boost/graph/adjacency_list.hpp>
#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <stdexcept>
#include "Const.h"
#include "Vertex.h"
#include "Edge.h"

typedef boost::adjacency_list<
        boost::listS,
        boost::vecS,
        boost::bidirectionalS,
        Vertex,
        Edge
> graph_t;  // the graph type

typedef boost::graph_traits<graph_t>::vertex_descriptor vertex_t;
typedef boost::graph_traits<graph_t>::out_edge_iterator out_edge_iterator_t;

class Graph {
 private:
    graph_t instance;  // the instance of the graph
    std::map<int64_t, vertex_t> verticesMap;  // the map associating each vertex identifier to a vertex descriptor

 public:
    /**
     * Constructor of a graph object
     */
    Graph() {}

    /**
     * Destructor of a graph object
     */
    ~Graph();

    /**
     * Getter of the graph instance
     * @return the instance of the graph
     */
    const graph_t &getInstance() const {
        return instance;
    }

    /**
     * Getter of the vertex descriptor associated with a vertex identifier
     * @param vertexId a vertex identifier
     * @return the vertex descriptor
     */
    const vertex_t &getVertex(const int64_t vertexId) const {
        return verticesMap.find(vertexId)->second;
    }

    /**
     * Setter of graph vertices
     * @param vertices
     */
    void setVertices(std::vector<Vertex> vertices);

    /**
     * Setter of graph edges
     * @param edges
     */
    void setEdges(std::vector<Edge> edges);

    /**
     * Method to print graph instance data
     * Number of vertices
     * Number of edges
     */
    void printData() const;
};


#endif  // INCLUDE_GRAPH_H_
