/*
 * Copyright 2017 Giret
 * */

#include "../include/LSDPF_Dijkstra_Visitor.h"

u_int8_t nbTimesTargetReached = 0;
bool allTargetReached = false;
std::vector<double> worstCriteriaValues = std::vector<double>(NB_CRITERIA_COST + NB_CRITERIA_CONSTRAINED + NB_CRITERIA_MIN_MAX, 0.);
boost::signals2::signal<void()> onTargetReached;

