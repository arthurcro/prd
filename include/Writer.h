/*
 * Copyright 2017 Giret
 * */

#ifndef INCLUDE_WRITER_H_
#define INCLUDE_WRITER_H_

#include <boost/filesystem.hpp>
#include <string>
#include <fstream>
#include "Graph.h"
#include "LSDPF.h"
#include "Instance.h"

class Writer {
 private:
    std::string resultsFilesDir;  // the directory in which to write results files
    std::ofstream resultsFile;  // the results file stream

 public:
    /**
     * Constructor of a writer object
     * @param _resultFilesDir the directory in which to write results
     */
    explicit Writer(const std::string &_resultFilesDir);

    /**
     * Destructor of a writer object
     */
    ~Writer();

    /**
     * Getter of the current time
     * @return the current time formatted
     */
    const std::string getCurrentTimeFormatted();

    /**
     * Method to append the resolved instance results to the results file
     * @param instance
     * @param lsdpf
     */
    void writeResults(const Instance &instance, const LSDPF *lsdpf);

    /**
     * Method to write the file containing the Pareto front of the resolved instance
     * @param instance
     * @param lsdpf
     */
    void writeParetoFrontFile(const Instance &instance, const LSDPF *lsdpf);
};

#endif  // INCLUDE_WRITER_H_
