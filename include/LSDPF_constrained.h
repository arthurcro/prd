//
//  LSDPF_constrained.hpp
//  label_setting
//
//  Created by Arthur Crocquevieille on 10/01/2018.
//

#ifndef LSDPF_constrained_h
#define LSDPF_constrained_h

#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/property_map/property_map.hpp>
#include <boost/property_map/transform_value_property_map.hpp>
#include <boost/graph/reverse_graph.hpp>
#include <boost/multi_index_container.hpp>
#include <boost/multi_index/mem_fun.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/thread.hpp>
#include <boost/timer/timer.hpp>
#include <boost/signals2.hpp>
#include <iostream>
#include <vector>
#include <limits>
#include <memory>
#include <cmath>
#include "Graph.h"
#include "LSDPF_constrained_Dijkstra_Visitor.hpp"
#include "Label.h"
#include "Const.h"

typedef boost::multi_index_container<
    Label,
    boost::multi_index::indexed_by<
        boost::multi_index::ordered_non_unique<
        boost::multi_index::tag<Label::ByCriteriaValues>,
        boost::multi_index::const_mem_fun<Label, const std::vector<double>&, &Label::getCriteriaValues>
    >,
    boost::multi_index::ordered_unique<
        boost::multi_index::tag<Label::ById>,
        boost::multi_index::const_mem_fun<Label, int64_t, &Label::getId>
    >
    >,
std::allocator<Label>
> vertex_label_list_t;


typedef boost::multi_index_container<
    Label,
    boost::multi_index::indexed_by<
        boost::multi_index::ordered_non_unique<
            boost::multi_index::tag<Label::ByCriteriaValues>,
            boost::multi_index::const_mem_fun<Label, const std::vector<double>&, &Label::getCriteriaValues>
        >,
        boost::multi_index::ordered_non_unique<
            boost::multi_index::tag<Label::ByProjectedCriteriaValues>,
            boost::multi_index::const_mem_fun<
                Label, const std::vector<double>&, &Label::getProjectedCriteriaValues>>,
        boost::multi_index::ordered_unique<
            boost::multi_index::tag<Label::ById>,
            boost::multi_index::const_mem_fun<Label, int64_t, &Label::getId>
        >
    >,
    std::allocator<Label>
> exploration_queue_t;

struct Stats_constrained  {
    unsigned int firstPhaseTime;
    unsigned int secondPhaseTime;
    int64_t nbCreatedLabels;
    int64_t nbLabelsAddedToPF = 0;
};

class LSDPF_constrained {
    
private :
    
    /*
     * the structure containing criteria values between the target and each vertex,
     * for each reversed Dijkstra search
     */
    std::vector<std::vector<std::vector<double>>> criteriaValues;
    
    const graph_t &graph; // the graph in which we launch the search
    const vertex_t &source; // the source descriptor
    const vertex_t &target; // the target descriptor
    Stats_constrained stats;  // the structure containing statistics of the this LSDPF instance
    vertex_label_list_t* vertexLabelList;
    exploration_queue_t explorationQueue;
    int64_t labelId;  // the label identifier

    /**
     * Method to get the next label identifier
     * @return the next label identifier
     */
    int64_t getNextLabelId();
    /**
     * Method to get the next label to extend from the exploration queue
     * @tparam Tag le label selection strategy
     * @return the next label to extend
     */
    template<typename Tag>
    Label getNextLabel();
    /**
     * Method to determine if a set of criteria values is dominated on a vertex
     * @param criteriaValues the criteria values to compare
     * @param vertex the vertex containing the labels to test
     * @return true if the set of criteria values is dominated, false otherwise
     */
    bool isDominated(const std::vector<double> &criteriaValues, vertex_t vertex) const;

    /**
     * Method to purge dominated labels on a vertex
     * @param label the label used to purge, associated to a vertex
     * @param dominatedLabels the set of dominated labels
     */
    void purgeDominated(const Label &label, std::vector<Label> &dominatedLabels);
    
    /**
     * Method to get elapsed time of a timer
     * @param timer the timer
     * @return the elapsed time
     */
    unsigned int getElapsedTime(const boost::timer::cpu_timer &timer) const;
    
    /**
     * Method to add a label to a vertex
     * @param label the label to add
     */
    void addLabelToVertex(const Label &label);
public:
    
    /**
     * Constructor of a LSDPF_constrained objetc
     * It RESUME
     * @param _graph the graph in which we launch the search
     * @param _sourceId the source identifier
     * @param _targetId the target identifier
     */
    LSDPF_constrained(const Graph *_graph, int64_t _sourceId, int64_t _targetId);
    /**
     * Destructor of a LSDPF_constrained object
     */
    ~LSDPF_constrained();
    
    /**
     * Getter of the structure containing statistics of the this LSDPF_constrained instance
     * @return statistics of the this LSDPF_constrained instance
     */
    const Stats_constrained &getStats() const {
        return stats;
    }
    
    /**
     * Getter of the Pareto front
     * @return the Pareto front
     */
    const vertex_label_list_t &getParetoFront() const {
        return vertexLabelList[target];
    }
    /**
     * Method to run the LSDPF_constrained first phase
     * First phase aims to
     * - compute lower and upper bounds on each vertex for second phase
     * - find some initial solutions for the Pareto front
     */
    void runFirstPhase();
    
    /**
     * Method to run a reversed Dijkstra search from target to all interesting vertices
     * @param reverseGraph the reversed graph instance
     * @param n the search index
     */
    void runReversedDijkstra(const boost::reverse_graph<graph_t> &reverseGraph, u_int8_t n);
    
    /**
     * Method to run the LSDPF_constrained second phase
     * Second phase aims to find all non-dominated solutions
     */
    void runSecondPhase();
    
    /**
     * Method to print LSDPF_constrained statistics
     */
    void printStatistics() const;
    
    /**
     * Method to print LSDPF_constrained statistics
     */
    void printStatistics(int64_t instanceId) const;
};
#endif /* LSDPF_constrained_h */
