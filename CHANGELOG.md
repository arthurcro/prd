## v1.2.0

* Add a writer for results files
* Add program argument for results directory path

## v1.1.0

* Add a parser for instances files
* Add program arguments for vertices file path, edges file path and instances file path