/*
 * Copyright 2017 Giret
 * */

#ifndef INCLUDE_VERTEX_H_
#define INCLUDE_VERTEX_H_

#include <osmium/osm/location.hpp>

class Vertex {
 public:
    const int64_t id;  // the identifier of the vertex

 private:
    const osmium::Location location;  // the location of the vertex

 public:
    /**
     * Constructor of a null vertex object
     */
    Vertex() : id(0), location(osmium::Location()) {}

    /**
     * Constructor of a vertex object
     * @param _id the identifier of the vertex
     * @param _location the location of the vertex
     */
    Vertex(const int64_t _id, const osmium::Location &_location) : id(_id), location(_location) {}
};

#endif  // INCLUDE_VERTEX_H_
